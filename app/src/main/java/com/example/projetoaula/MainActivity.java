package com.example.projetoaula;

import androidx.appcompat.app.AppCompatActivity;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity {

    TextView nomeUser;
    Button btnPedidos;
    Button btnDadosPessoais;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);



        Bundle bundle = getIntent().getExtras();
        String user = bundle.getString("user");

        /*
        * referencia ao botão dash dados pessoais
        * */
        btnDadosPessoais = findViewById(R.id.btndadosPessoais);
        btnDadosPessoais.setOnClickListener(actDadosPessoais());
    }

    private View.OnClickListener actDadosPessoais() {
        return new Button.OnClickListener(){
            @Override
            public void onClick(View view){
                startActivity(new Intent(getActivity(), DadosPessoaisActivity.class));
            }
        };
    }

    /*
    * Método Referente a botão dash meus pedidos
    * */
    public void actPedidos(View view) {

        startActivity(new Intent(getActivity(), MeusPedidosActivity.class));
    }

    public Context getActivity() {
        return this;
    }

    public void actProdutos(View view) {
        startActivity(new Intent(getActivity(), ProdutosActivity.class));
    }
}
