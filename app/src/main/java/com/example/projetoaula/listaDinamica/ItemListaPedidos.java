package com.example.projetoaula.listaDinamica;

public class ItemListaPedidos {

    private int id;
    private String data;
    private double valor;
    private int img;

    public int getImg() {
        return img;
    }

    public void setImg(int img) {
        this.img = img;
    }

    public ItemListaPedidos(int id, String data, double valor, int img) {
        this.id = id;
        this.data = data;
        this.valor = valor;
        this.img = img;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getData() {
        return data;
    }

    public void setData(String data) {
        this.data = data;
    }

    public double getValor() {
        return valor;
    }

    public void setValor(double valor) {
        this.valor = valor;
    }
}
