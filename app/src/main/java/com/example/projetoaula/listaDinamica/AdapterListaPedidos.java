package com.example.projetoaula.listaDinamica;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.projetoaula.R;

import java.util.List;

public class AdapterListaPedidos extends BaseAdapter {

    LayoutInflater inflater;
    List<ItemListaPedidos> itens;

    public AdapterListaPedidos(Context context, List<ItemListaPedidos> itens) {
        this.inflater = LayoutInflater.from(context);
        this.itens = itens;
    }

    @Override
    public int getCount() {
        return itens.size();
    }

    @Override
    public Object getItem(int position) {
        return itens.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        ItemListaPedidos item = itens.get(position);

        convertView = inflater.inflate(R.layout.item_lista_pedidos, null);

        ((TextView)convertView.findViewById(R.id.txtId)).setText(String.valueOf(item.getId()));
        ((TextView)convertView.findViewById(R.id.txtData)).setText(item.getData());
        ((TextView)convertView.findViewById(R.id.txtValor)).setText(String.valueOf(item.getValor()));
        ((ImageView)convertView.findViewById(R.id.imgDetalhes)).setImageResource(item.getImg());
        return convertView;
    }
}
