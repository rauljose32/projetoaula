package com.example.projetoaula;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.MenuItem;
import android.widget.ArrayAdapter;
import android.widget.ListView;

import com.example.projetoaula.com.example.entity.Cliente;

import java.util.ArrayList;
import java.util.List;

public class DadosPessoaisActivity extends AppCompatActivity {



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_dados_pessoais);
        //ativar opção de retorno
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        //titulo activity
        getSupportActionBar().setTitle("Dados Pessoais");


    }
    /*
     * Função para setDisplayHomeAsUpEnabled
     *
     * */
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        switch (item.getItemId()){
            case android.R.id.home:
                finish();
                break;
        }

        return super.onOptionsItemSelected(item);
    }
}
