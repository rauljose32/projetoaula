package com.example.projetoaula;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.MenuItem;
import android.widget.ArrayAdapter;
import android.widget.ListView;

import com.example.projetoaula.com.example.entity.Cliente;
import com.example.projetoaula.com.example.entity.Produto;

import java.util.ArrayList;
import java.util.List;

public class ProdutosActivity extends AppCompatActivity {

    ListView lsvProdutos;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_produtos);

        //ativar opção de retorno
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        //titulo activity
        getSupportActionBar().setTitle("Meus Produtos");

        lsvProdutos = findViewById(R.id.lsvProdutos);
        List<String> produtos = new ArrayList();

        Produto p1 = new Produto(1, "Arroz",2.50);
        Produto p2 = new Produto(2,"Feijão", 3.50);
        Produto p3 = new Produto(3,"Macarrão", 3.50);

        produtos.add(p1.getDescricao());
        produtos.add(p2.getDescricao());
        produtos.add(p3.getDescricao());

        ArrayAdapter arrayAdapter = new ArrayAdapter(
                this,android.R.layout.simple_list_item_1,produtos);

        lsvProdutos.setAdapter(arrayAdapter);
    }
    /*
     * Função para setDisplayHomeAsUpEnabled
     * */
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        switch (item.getItemId()){
            case android.R.id.home:
                finish();
                break;
        }

        return super.onOptionsItemSelected(item);
    }
}
