package com.example.projetoaula;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

public class LoginActivity extends AppCompatActivity {

    EditText edtUsuario;
    EditText edtSenha;
    Button btnEntrar;
    Button btnCancelar;
    SharedPreferences preferences;


    @Override
    protected void onCreate(final Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        String usuario, senha;
        edtUsuario = findViewById(R.id.edtUsuario);
        edtSenha = findViewById(R.id.edtSenha);
        btnEntrar = findViewById(R.id.btnEntrar);
        btnCancelar = findViewById(R.id.btnCancelar);

        preferences = getSharedPreferences("login", Context.MODE_PRIVATE);
        usuario = preferences.getString("user", null);
        senha = preferences.getString("pass", null);

        if(usuario != null && senha != null) {
            Intent intent = new Intent(getActivity(), MainActivity.class);
            intent.putExtra("user", usuario);
            startActivity(intent);
            finish();
        }


        /*
         * Imprimir msg
         * */
        btnEntrar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                String usuarioA = edtUsuario.getText().toString();
                String senhaA = edtSenha.getText().toString();

                if (usuarioA.equals("aluno") && senhaA.equals("1234")) {

                    SharedPreferences.Editor editor = preferences.edit();
                    editor.putString("user", usuarioA);
                    editor.putString("pass", senhaA);
                    editor.commit();

                    Intent intent = new Intent(getActivity(), MainActivity.class);
                    intent.putExtra("user", edtUsuario.getText().toString());
                    startActivity(intent);
                    finish();

                } else {
                    Toast.makeText(getActivity(),
                            "Usuario ou Senha incorreta",
                            Toast.LENGTH_SHORT).show();
                    edtUsuario.requestFocus();
                }
            }

        });
    }

    public Context getActivity() {
        return this;
    }

}
