package com.example.projetoaula;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.MenuItem;
import android.widget.Button;
import android.widget.ListView;

import com.example.projetoaula.listaDinamica.AdapterListaPedidos;
import com.example.projetoaula.listaDinamica.ItemListaPedidos;

import java.util.ArrayList;
import java.util.List;

public class MeusPedidosActivity extends AppCompatActivity {

    ListView listView;
    AdapterListaPedidos adapterListaPedidos;
    List<ItemListaPedidos> itemListaPedidosList = new ArrayList();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_meus_pedidos);

        //ativar opção de retorno
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        //titulo activity
        getSupportActionBar().setTitle("Meus Pedidos");




        listView = findViewById(R.id.lsvMeusPedidos);

        ItemListaPedidos item1 = new ItemListaPedidos(1, "27/07/19", 50, R.mipmap.zoom16);
        ItemListaPedidos item2 = new ItemListaPedidos(2, "28/07/19", 150, R.mipmap.zoom16);
        ItemListaPedidos item3 = new ItemListaPedidos(3, "29/07/19", 250,R.mipmap.zoom16);

        itemListaPedidosList.add(item1);
        itemListaPedidosList.add(item2);
        itemListaPedidosList.add(item3);

        adapterListaPedidos = new AdapterListaPedidos(this,itemListaPedidosList);
        listView.setAdapter(adapterListaPedidos);

    }
    /*
    * Função para setDisplayHomeAsUpEnabled
    * */
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        switch (item.getItemId()){
            case android.R.id.home:
                finish();
                break;
        }

        return super.onOptionsItemSelected(item);
    }
}
